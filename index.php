<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BigNumberCalculate</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="../BTLCĐN/css/styles.css">
    <link rel="stylesheet" href="../BTLCĐN/css/style.css">

</head>

<body>

    <div class="main">
        <div class="container">
            <div class="row">

                <div class="tab" role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#Section1" aria-controls="home" role="tab" data-toggle="tab">Tính Toán Số Nguyên Lớn</a></li>
                        <li role="presentation"><a href="#Section2" aria-controls="profile" role="tab" data-toggle="tab">Tính Trung Bình Lương</a></li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content tabs">
                        <div role="tabpanel" class="tab-pane fade in active Section1" id="Section1">
                            <h3 class="title">
                                PHẦN MỀM TÍNH SỐ NGUYÊN LỚN
                            </h3>
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 resultSection">
                                        <label for="number1">Số Thứ Nhất:</label><br>
                                        <!-- <input type="text" maxlength="100" id="firstNum"  name="number1" class="input"><br> -->
                                        <textarea name="number1" id="firstNum" cols="65" rows="3" class="ui-autocomplete-input textareaInput"></textarea>
                                        <label for="number1">Số Thứ Hai:</label><br>
                                        <!-- <input type="text" maxlength="100" id="secondNum"  name="number1" class="input"><br> -->
                                        <textarea name="number1" id="secondNum" cols="65" rows="3" class="ui-autocomplete-input textareaInput"></textarea>
                                        <p>Kết quả</p>
                                        <textarea name="" id="result" cols="65" rows="3" class="result textareaInput" readonly></textarea>
                                        <p>Thời gian thực thi (ms):</p>
                                        <input type="text" readonly id="runningTime"><br>

                                    </div>
                                    <div class="col-xs-12 col-sm-3 inputSection">
                                        <button type="button" class="btn btn-primary Addition" onclick="calculate('addition')">Cộng
                                            Hai Số Nguyên Lớn</button>
                                        <button type="button" class="btn btn-warning Subtraction" onclick="calculate('subtraction')">Trừ Hai Số Nguyên Lớn</button>
                                        <button type="button" class="btn btn-info Multiply" onclick="calculate('multiply')">Nhân
                                            Hai Số Nguyên Lớn</button>
                                        <button type="button" class="btn btn-danger Division" onclick="calculate('division')">Chia
                                            Hai Số Nguyên Lớn</button>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-success randomNum" data-toggle="modal" data-target="#exampleModal">
                                            Sinh Số Ngẫu Nhiên
                                        </button>
                                        <button type="button" class="btn btn-default clearAll" onclick="clearAll()">Clear all</button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel"  style="color: black; text-align:center">Mời bạn nhập phạm vi số ngẫu nhiên</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <label for="firstNum" style="color: black">Số thứ nhất:</label>
                                                        <input type="text" name="firstNum" id="number1" style="color: black"><br>
                                                        <label for="secondNum" style="color: black">Số thứ hai:</label>
                                                        <input type="text" name="secondNum" id="number2" style="color: black; 
    margin-left: 9px;
"><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="randomNumber()">Ok bạn nhé :3</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 

                                </div>
                            </div>
                        </div>
               
                    <div role="tabpanel" class="tab-pane fade Section2" id="Section2">
                        <h3 class="title">Ứng Dụng Quản Lý Lương Cán Bộ</h3>
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12 col-sm-7 leftSide">


                                    <div class="topSection">
                                        <h5 class="numberofRows">Số bản ghi muốn hiển thị</h5>
                                        <div class="form-group">
                                            <!--		Show Numbers Of Rows 		-->
                                            <select class="form-control" name="state" id="maxRows">
                                                <option value="5000">Show ALL Rows</option>
                                                <option value="5">5</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="20">20</option>
                                                <option value="50">50</option>
                                                <option value="70">70</option>
                                                <option value="100">100</option>
                                            </select>

                                        </div>
                                        <form action="export.php" method="post" enctype="multipart/form-data">
                                            <table class="table table-striped table-class" id="table-id">
                                                <thead>
                                                    <tr>
                                                        <th>Mã Cán Bộ</th>
                                                        <th>Tên Cán Bộ</th>
                                                        <th>Lương</th>
                                                    </tr>
                                                </thead>
                                                <?php
                                                $servername = "localhost";
                                                $username = "root";
                                                $password = "";
                                                $dbname = "btlttcđn";

                                                // Create connection
                                                $conn = new mysqli($servername, $username, $password, $dbname);
                                                // Check connection
                                                if ($conn->connect_error) {
                                                    die("Connection failed: " . $conn->connect_error);
                                                }

                                                $sql = "SELECT * FROM tblcanbo";
                                                $result = $conn->query($sql);

                                                if ($result->num_rows > 0) {
                                                    // output data of each row
                                                  //  $arr = array();
                                                    while ($row = $result->fetch_assoc()) {
                                                        echo
                                                            '<tr>
                                                                <td scope="row" name="iMaCanBo">' . $row["iMaCanBo"] . '</td>
                                                                <td name="sTenCanBo">' . $row["sTenCanBo"] . '</td>
                                                                <td id="salary" name="Luong">' . number_format($row["Luong"]) . '</td>
                                                            </tr>';                                                       
                                                      //  array_push($arr, $row["Luong"]);
                                                       
                                                    }
                                                    //print_r($arr);

                                                }
                                                $conn->close();
                                                ?>


                                            </table>

                                            <!--		Start Pagination -->
                                            <div class='pagination-container'>
                                                <nav>
                                                    <ul class="pagination">

                                                        <li data-page="prev">
                                                            <span>
                                                                < <span class="sr-only">(current)
                                                            </span></span>
                                                        </li>
                                                        <!--	Here the JS Function Will Add the Rows -->
                                                        <li data-page="next" id="prev">
                                                            <span> > <span class="sr-only">(current)</span></span>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                    </div>
                                    <section class="bottomSection">
                                        <?php
                                        $connect = mysqli_connect("localhost", "root", "", "btlttcđn");
                                        $query = "SELECT * FROM tblCanBo";
                                        $query_run = mysqli_query($connect, $query);

                                        $qty = 0;
                                        $count = 0;
                                        $arr = array();
                                        while ($num = mysqli_fetch_assoc($query_run)) {
                                           // $qty += $num['Luong'];
                                            array_push($arr, $num["Luong"]);
                                            $count++;
                                        }
                    
                                        $firstIndex = $arr[0];
                                
                                        for($i = 1; $i < sizeof($arr); $i++){
                                            $diff = strlen($firstIndex) - strlen($arr[$i]);
                                            if($diff == 0){
                                                $firstIndex += $arr[$i];
                                            }
                                            if($diff > 0){
                                                //thêm số 0 tương ứng vào chuỗi có độ dài nhỏ hơn chuỗi còn lại cho 2 chuỗi bằng nhau
                                                $newArr1 = str_pad($arr[$i],strlen($firstIndex),"0",STR_PAD_LEFT) ;
                                                $firstIndex += $newArr1;
                                            }
                                        
                                            if($diff < 0){
                                                 //thêm số 0 tương ứng vào chuỗi có độ dài nhỏ hơn chuỗi còn lại cho 2 chuỗi bằng nhau
                                                $newArr2 = str_pad($firstIndex,strlen($arr[$i]),"0",STR_PAD_LEFT) ;
                                                $firstIndex = $newArr2 + $arr[$i];                     
                                            }
                                        }
                                        $avr = $firstIndex / $count;
                                        $averageOfSalary = number_format($avr);

                                        $sql = "INSERT INTO tbl_income (tongLuong, trungBinhLuong) values ('$firstIndex', '$averageOfSalary');";
                                        if (mysqli_query($connect, $sql)) { 
                                            echo '   
                                                        <h5>Tổng Lương:
                                                            <input type="text" name="" maxlength="100" class="sumOfSalary" id="sumOfSalary" value="' . number_format($firstIndex) . '" readonly>
                                                        </h5>
                                                       <h5>Trung Bình Lương là:
                                                           <input type="text" name="" maxlength="100" class="sumOfSalary" id="averageOfSalary" value="' . $averageOfSalary . '" readonly>
                                                       </h5>
                                                   ';
                                        } else {
                                            echo "ERROR: Could not able to execute $sql. " . mysqli_error($connect);
                                        }

                                        ?>


                                    </section>
                                </div>
                                <div class="col-xs-12 col-sm-2 rightSide">

                                    <input type="file" name="file" class="btnUpload">
                                    <input type="submit" name="import" class="btn btn-primary Addition" value="Gửi dữ liệu">
                                    <input type="submit" name="export" class="btn btn-warning Subtraction" value="Xuất file CSV">
                                    <!-- <input type="submit" name="sumOfSalary" class="btn btn-info Multiply" value="Tính Tổng Lương">
                                       <input type="submit" name="averageOfSalary" class="btn btn-danger Division" value="Tính TB Lương"> -->

                                </div>
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>
    </div>


</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous">
</script>


<script src="../BTLCĐN/js/BigNumberCalculate.js"></script>

</html>