////////----------Hàm calculate-------------/////////////
function calculate(type) {
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    var num1 = document.getElementById("firstNum").value;
    var num2 = document.getElementById("secondNum").value;
    var value = "";
    if(RE.test(num1) && RE.test(num2)){
        switch (type) {
            case "addition":
                value = addition(num1, num2);
                break;
            case "subtraction":
                value = subtraction(num1, num2);
                break;
            case "multiply":
                value = multiply(num1, num2);
                break;
            case "division":
                value = division(num1, num2);
                break;
        }
    }else return alert("Dữ liệu đầu vào không thỏa mãn");
   
   

    var result = value.length === 0 ? "0" : value;

    document.getElementById("result").innerHTML = result;
}

////////----------Hàm Tính Nhân-------------////////////
function multiply(num1, num2) {
   
    var start = performance.now();
     //biến để kiểm tra xem kết quả âm hay dương
    let negative = false;

    if ((num1.charAt(0) != '-' && num2.charAt(0) === '-') || (num1.charAt(0) === '-' && num2.charAt(0) != '-')) {
        negative = true;
    }

    //xóa hết dấu âm
    num1 = num1.replace('-', '');
    num2 = num2.replace('-', '');

    let m = num1.length,
        n = num2.length;

    if (num1.replace('0', '') == ''||num2.replace('0', '') == '') {
        return "0";
    }

    //khai báo một mảng mới với m+n phần tử, và khởi tạo giá trị mặc định các phần tư là 0
    let res = Array(m + n).fill(0)

    for (let i = m - 1; i >= 0; i--) {
        for (let j = n - 1; j >= 0; j--) {
            let temp = num1.charAt(i) * num2.charAt(j)
            let posLow = i + j + 1
            let posHigh = i + j
            temp += res[posLow]
            res[posLow] = temp % 10
            //Hàm Math.trunc() trả về giá trị nguyên của đối số truyền vào
            res[posHigh] += Math.trunc(temp / 10)
        }
    }
    while (res[0] === 0) {
        //Hàm shift() xóa phần tử đầu tiên của mảng res
        res.shift()
    }

    var result = res.length === 0 ? "0" : res.join('');
    //thêm dấu âm nếu cần
    if (negative) result = '-' + result;

    var per = performance.now() - start;
    document.getElementById("runningTime").value = per;

    return result;
};

/////////--------Hàm Tính Chia----------//////////

function division(num1, num2) {
    var start = performance.now();
    //biến để kiểm tra xem kết quả âm hay dương
    let negative = false;

    if ((num1.charAt(0) != '-' && num2.charAt(0) === '-') || (num1.charAt(0) === '-' && num2.charAt(0) != '-')) {
        negative = true;
    }

    //xóa hết dấu âm
    num1 = num1.replace('-', '');
    num2 = num2.replace('-', '');

    var ans = "";

    if (num2.replace('0', '') == '') {
        ans = "Không hợp lệ, số chia phải khác 0";
        negative = false;
    } else {
        if (num1.replace('0', '') == '') {
            ans = "0";
            negative = false;
        } else {
            if (num1 == num2) {
                ans = "1";
            } else {
                let phandu = "";
                let temp = num1.substring(0, num2.length);
                let subNum1 = temp;
                let flag = true;

                while (ans.length <= 100) {

                    for (let i = 1; i <= 10; i++) {
                        let mul = multiply(num2, i.toString());

                       
                        if (numBigger(temp, mul) == mul) {
                           
                            ans += (i - 1);
                           
                            phandu = subtraction(temp, multiply(num2, (i - 1).toString()));
                          
                            break;
                        }
                    }

                    if (subNum1.length < num1.length) {
                        if (phandu == 0) phandu = "";
                        // console.log("sub "+subNum1);
                        subNum1 = num1.substring(0, subNum1.length + 1);
                        temp = phandu + num1.substring(subNum1.length - 1, subNum1.length);
                        console.log("sub " + subNum1);
                        console.log("temp " + temp);
                    } else {
                        if (phandu == 0) {
                            break;
                        } else {
                            if (flag) {
                                ans += ".";
                                flag = false;
                            }
                            temp = phandu + "0";
                        }
                    }
                }
            }
        }
    }

    let res = ans.split('');
    while (res[0] == 0 && res[1] != '.') {
        //Hàm shift() xóa phần tử đầu tiên của mảng res
        res.shift()
    }

    var result = res.length === 0 ? "0" : res.join('');
    //thêm dấu âm nếu cần
    if (negative) result = '-' + result;

    var per = performance.now() - start;
    document.getElementById("runningTime").value = per;

    return result;
}

/////////------------Hàm Tính Cộng------------------/////////////////////

function addition(num1, num2) {
    var start = performance.now();
    if ((num1.charAt(0) != '-' && num2.charAt(0) === '-') || (num1.charAt(0) === '-' && num2.charAt(0) != '-')) {
        if (num1.charAt(0) === '-') {
            return subtraction(num2, num1.replace('-', ''));
        } else {
            return subtraction(num1, num2.replace('-', ''));
        }
    } else {
        //biến để kiểm tra xem kết quả cần đảo dấu
        let negative = false;

        if (num1.charAt(0) === '-') {
            negative = true;
        }

        //xóa hết dấu âm
        num1 = num1.replace('-', '');
        num2 = num2.replace('-', '');

        //convert String to Array
        var Arr1 = num1.split('');
        var Arr2 = num2.split('');
        //map(Number) lấy từng giá trị trong mảng xử lý và trả về kết quả theo kiểu đối tượng Number và truyền các tham số tới Number
        var res = Arr1.map(Number);
        var sub = Arr2.map(Number);
        var reminder = 0;

        let diff = res.length - sub.length;
        if (diff > 0) {
            addZeroes(sub, diff)
        } else if (diff < 0) {
            ///Math.abs() trả về giá trị tuyệt đối của một số
            addZeroes(res, Math.abs(diff));
        }
        let count = res.length - 1
        while (count >= 0) {
            let sum = res[count] + sub[count] + reminder;
            res[count] = sum % 10; //Lấy giá trị hàng đơn vị, chia dư lấy 10
            reminder = Math.floor(sum / 10); //Lấy giá trị nhớ reminder, dùng math floor lấy giá trị nguyên làm tròn phép chia của kết quả vừa được / cho 10

            count--;
        }

        if (reminder === 1) {
            res.unshift(1);
        }

        while (res[0] === 0) {
            res.shift();
        }

        if (negative && res.length != 0) {
            res.unshift(0);
            res[0] = '-';
        }

        //convert những giá trị của mảng res sang string
        var result = res.length === 0 ? "0" : res.join('');
       
        var per = performance.now() - start;
        document.getElementById("runningTime").value = per;

        return result;
       
    }
}

//////-------------HÀM TÍNH TRỪ----------------//////////////
function subtraction(num1, num2) {
    var start = performance.now();

    if ((num1.charAt(0) != '-' && num2.charAt(0) === '-') || (num1.charAt(0) === '-' && num2.charAt(0) != '-')) {
        if (num1.charAt(0) === '-') {
            return addition(num1, '-' + num2);
        } else {
            return addition(num1, num2.replace('-', ''));
        }
    } else {

        //biến để kiểm tra xem kết quả cần đảo dấu
        let negative = false;

        if (num1.charAt(0) === '-') {
            negative = true;
        }

        //xóa hết dấu âm
        num1 = num1.replace('-', '');
        num2 = num2.replace('-', '');

        //convert String to Array
        var Arr1 = num1.split('');
        var Arr2 = num2.split('');
        //map(Number) lấy từng giá trị trong mảng xử lý và trả về kết quả theo kiểu đối tượng Number và truyền các tham số tới Number
        var res = Arr1.map(Number);
        var sub = Arr2.map(Number);

        let reminder = 0;
        let diff = res.length - sub.length;

        if (numBigger(num1, num2) == num1 || numBigger(num1, num2) == "") {
            addZeroes(sub, diff)
            let count = res.length - 1
            while (count >= 0) {
                let sum = res[count] - sub[count] - reminder;
                if (sum < 0) {
                    res[count] = sum + 10
                    reminder = 1
                } else {
                    res[count] = sum
                    reminder = 0
                }
                count--;
            }

            while (res[0] === 0) {
                res.shift();
            }

            if (negative && res.length != 0) {
                res.unshift(0);
                res[0] = '-';
            }

        } else {
            ///Math.abs() trả về giá trị tuyệt đối của một số
            addZeroes(res, Math.abs(diff))
            let count = res.length - 1
            while (count >= 0) {
                let sum = sub[count] - res[count] - reminder;
                if (sum < 0) {
                    res[count] = sum + 10
                    reminder = 1
                } else {
                    res[count] = sum
                    reminder = 0
                }
                count--;
            }

            while (res[0] === 0) {
                res.shift();
            }

            if (!negative && res.length != 0) {
                res.unshift(0);
                res[0] = '-';
            }
        }

        var result = res.length == 0 ? "0" : res.join('');

        var per = performance.now() - start;
        document.getElementById("runningTime").value = per;

        return result;
    }
}

function numBigger(num1, num2) {
    let numBig = "";
    if (num1.length > num2.length) {
        numBig = num1;
    } else if (num2.length > num1.length) {
        numBig = num2;
    } else if (num1.length == num2.length) {
        for (let i = 0; i < num1.length; i++) {
            if (num1.charAt(i) > num2.charAt(i)) {
                numBig = num1;
                break;
            } else if (num1.charAt(i) < num2.charAt(i)) {
                numBig = num2;
                break;
            }
        }
    }

    return numBig;
}



function addZeroes(arr, k) {
    let count = 0;
    while (count < k) {
        //Phương thức unshift() thêm 0 vào đầu mảng
        arr.unshift(0);
        count++;
    }
}


////////////////////----------Sinh Số Ngẫu Nhiên-------///////////////
function randomNumber() {
    //random từ 100 -> 1000 chữ số
    var so1 = document.getElementById("number1").value;
    var so2 = document.getElementById("number2").value;
    var input1 = parseInt(so1, 10); //6
    var input2 = parseInt(so2, 10);  //10
    var firstNumber = Math.floor(Math.random() * input2);
    var secondNumber = Math.floor(Math.random() * input2);
    if(firstNumber <= input1){
        num1Length = input2;
    }else {
        num1Length = firstNumber;
    }
     if(secondNumber <= input1){
        num2Length = input2;
    }else {
        num2Length = secondNumber;
    }

    var biggerLength = num1Length >= num2Length ? num1Length : num2Length;

    var num1 = "";
    var num2 = "";
    ///tạo ra số ngẫu nhiên
    for (let i = 0; i < biggerLength; i++) {
        if (i < num1Length) {
            num1+= Math.floor(Math.random() * 10);
        }
        if (i < num2Length) {
            num2+= Math.floor(Math.random() * 10);
        }
    }

    ////Phương thức toLocaleString để show fullwide của number và tránh ký tự khoa học với số lớn.
    document.getElementById("firstNum").value = num1;
    document.getElementById("secondNum").value = num2;
}

///////////////////////////////////////////////////////
function clearAll() {
    document.getElementById("firstNum").value = '';
    document.getElementById("secondNum").value = '';
    document.getElementById("result").value = '';
}


//////////////------Phân Trang---------///////////////
getPagination('#table-id');


function getPagination(table) {
    var lastPage = 1;

    $('#maxRows')
        .on('change', function (evt) {

            lastPage = 1;
            $('.pagination')
                .find('li')
                .slice(1, -1)
                .remove();
            var trnum = 0; // reset tr counter
            var maxRows = parseInt($(this).val()); // get Max Rows from select option

            if (maxRows == 5000) {
                $('.pagination').hide();
            } else {
                $('.pagination').show();
            }

            var totalRows = $(table + ' tbody tr').length; // numbers of rows
            $(table + ' tr:gt(0)').each(function () {
                // each TR in  table and not the header
                trnum++; // Start Counter
                if (trnum > maxRows) {
                    // if tr number gt maxRows

                    $(this).hide(); // fade it out
                }
                if (trnum <= maxRows) {
                    $(this).show();
                } // else fade in Important in case if it ..
            }); //  was fade out to fade it in
            if (totalRows > maxRows) {
                // if tr total rows gt max rows option
                var pagenum = Math.ceil(totalRows / maxRows); // ceil total(rows/maxrows) to get ..
                //	numbers of pages
                for (var i = 1; i <= pagenum;) {
                    // for each page append pagination li
                    $('.pagination #prev')
                        .before(
                            '<li data-page="' +
                            i +
                            '">\
                                                <span>' +
                            i++ +
                            '<span class="sr-only">(current)</span></span>\
                                              </li>'
                        )
                        .show();
                } // end for i
            } // end if row count > max rows
            $('.pagination [data-page="1"]').addClass('active'); // add active class to the first li
            $('.pagination li').on('click', function (evt) {
                // on click each page
                evt.stopImmediatePropagation();
                evt.preventDefault();
                var pageNum = $(this).attr('data-page'); // get it's number

                var maxRows = parseInt($('#maxRows').val()); // get Max Rows from select option

                if (pageNum == 'prev') {
                    if (lastPage == 1) {
                        return;
                    }
                    pageNum = --lastPage;
                }
                if (pageNum == 'next') {
                    if (lastPage == $('.pagination li').length - 2) {
                        return;
                    }
                    pageNum = ++lastPage;
                }

                lastPage = pageNum;
                var trIndex = 0; // reset tr counter
                $('.pagination li').removeClass('active'); // remove active class from all li
                $('.pagination [data-page="' + lastPage + '"]').addClass('active'); // add active class to the clicked
                // $(this).addClass('active');					// add active class to the clicked
                limitPagging();
                $(table + ' tr:gt(0)').each(function () {
                    // each tr in table not the header
                    trIndex++; // tr index counter
                    // if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
                    if (
                        trIndex > maxRows * pageNum ||
                        trIndex <= maxRows * pageNum - maxRows
                    ) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    } //else fade in
                }); // end of for each tr in table
            }); // end of on click pagination list
            limitPagging();
        })
        .val(5)
        .change();

    // end of on select change

    // END OF PAGINATION
}




function limitPagging() {
    // alert($('.pagination li').length)

    if ($('.pagination li').length > 7) {
        if ($('.pagination li.active').attr('data-page') <= 3) {
            $('.pagination li:gt(5)').hide();
            $('.pagination li:lt(5)').show();
            $('.pagination [data-page="next"]').show();
        }
        if ($('.pagination li.active').attr('data-page') > 3) {
            $('.pagination li:gt(0)').hide();
            $('.pagination [data-page="next"]').show();
            for (let i = (parseInt($('.pagination li.active').attr('data-page')) - 2); i <= (parseInt($('.pagination li.active').attr('data-page')) + 2); i++) {
                $('.pagination [data-page="' + i + '"]').show();

            }

        }
    }
}

$(function () {
    // Just to append id number for each row
    $('table tr:eq(0)').prepend('<th> # </th>');

    var id = 0;

    $('table tr:gt(0)').each(function () {
        id++;
        $(this).prepend('<td>' + id + '</td>');
    });
});
