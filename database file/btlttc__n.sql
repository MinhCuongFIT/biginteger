-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 23, 2020 lúc 11:13 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `btlttcđn`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblcanbo`
--

CREATE TABLE `tblcanbo` (
  `iMaCanBo` int(11) NOT NULL,
  `sTenCanBo` varchar(30) NOT NULL,
  `Luong` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblcanbo`
--

INSERT INTO `tblcanbo` (`iMaCanBo`, `sTenCanBo`, `Luong`) VALUES
(1, 'Phan Minh Cuong', '10000000000000000'),
(2, 'Mark', '20012121212121221'),
(3, 'Johnson', '30033333333333333'),
(4, 'Trump', '40044444444444444'),
(5, 'Ngo Bao Chau', '50044444444444444'),
(6, 'Ngoc Trinh', '60000444444444444');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_income`
--

CREATE TABLE `tbl_income` (
  `id` int(11) NOT NULL,
  `tongLuong` varchar(200) NOT NULL,
  `trungBinhLuong` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tbl_income`
--

INSERT INTO `tbl_income` (`id`, `tongLuong`, `trungBinhLuong`) VALUES
(15, '210134787878787886', '35,022,464,646,464,644');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tblcanbo`
--
ALTER TABLE `tblcanbo`
  ADD PRIMARY KEY (`iMaCanBo`);

--
-- Chỉ mục cho bảng `tbl_income`
--
ALTER TABLE `tbl_income`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_income`
--
ALTER TABLE `tbl_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
